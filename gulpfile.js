const gulp = require('gulp');
const webpack = require('webpack-stream');
const named = require('vinyl-named');
const uglify = require('gulp-uglify');
const moment = require('moment');
const zip = require('gulp-vinyl-zip').zip;

gulp.task('default', ['copy', 'images', 'pack', 'images-content'], function() {
  console.log(`Build finished at ${moment().format()}`);
});

gulp.task('watch', ['default'], function () {
  gulp.watch('src/*', ['default']);
});

gulp.task('build-demos', function() {
  return gulp.src('demos-src/*/*.demo.js')
  .pipe(named())
  .pipe(webpack())
  .pipe(gulp.dest('demos-dist/'));
});

gulp.task('pack', function () {
  return gulp.src(['src/main.js'])
  .pipe(named())
  .pipe(webpack())
  .pipe(gulp.dest('dist/'));
});

gulp.task('copy', function () {
  return gulp.src(['src/*.css', 'src/*.html', 'src/material-ui.woff2'])
  .pipe(named())
  .pipe(gulp.dest('dist/'));
});

gulp.task('images', function () {
  return gulp.src(['src/images/*'])
  .pipe(named())
  .pipe(gulp.dest('dist/images/'));
});

gulp.task('images-content', function () {
  return gulp.src(['src/images/content/*'])
  .pipe(named())
  .pipe(gulp.dest('dist/images/content/'));
});

gulp.task('build', ['default'], function () {
  return gulp.src('dist/**/*')
  .pipe(zip('build.zip'))
  .pipe(gulp.dest('./'));
});
