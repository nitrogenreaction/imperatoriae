const fs = require('fs');
const moment = require('moment');

let inputArray = JSON.parse(fs.readFileSync('inputevents.json', 'utf8'));

const priorityTable = {
  'create-faction': 1,
  'delete-faction':2,
  'create-event': 3,
  'create-force': 3,
  'move-force': 4,
  'update-force': 4,
  'delete-force': 5,
  'delete-event': 5
}

inputArray.sort((a, b) => {
  let aTime = moment(a.data.time), bTime = moment(b.data.time);

  if (aTime.isBefore(bTime)) {
    return -1;
  } else if (aTime.isSame(bTime)) {
    return priorityTable[a.type] - priorityTable[b.type];
  } else {
    return 1;
  }
});

fs.writeFileSync('outputevents.json', JSON.stringify(inputArray, null, 2));
