let Faction = require('./faction');
let myMap = L.map('mapid').setView([51.505, -0.09], 13);
let moment = require('moment');

let eventStream = [{
  type: "create-force", data: {
    id: 'afag1',
    "viewer-faction": "allies",
    "owner-faction": "axis",
    "shape": {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              -1.23046875,
              49.61070993807422
            ],
            [
              -1.3128662109375,
              49.64984813025768
            ],
            [
              -1.40625,
              49.56263371687227
            ],
            [
              -1.22772216796875,
              49.29288913249991
            ],
            [
              -0.2581787109375,
              49.23912083246698
            ],
            [
              0.15106201171874997,
              49.28034843646649
            ],
            [
              0.296630859375,
              49.377008355425446
            ],
            [
              0.2801513671875,
              49.434198772487406
            ],
            [
              0.22521972656249997,
              49.434198772487406
            ],
            [
              0.12908935546875,
              49.40739905889802
            ],
            [
              0.0164794921875,
              49.337651296668845
            ],
            [
              -0.1043701171875,
              49.30184481970431
            ],
            [
              -0.2197265625,
              49.29109779978075
            ],
            [
              -0.318603515625,
              49.30184481970431
            ],
            [
              -0.4119873046875,
              49.337651296668845
            ],
            [
              -0.538330078125,
              49.350177413469
            ],
            [
              -0.7086181640625,
              49.350177413469
            ],
            [
              -0.9008789062499999,
              49.3787965643504
            ],
            [
              -0.9283447265625,
              49.3948875168789
            ],
            [
              -1.0107421875,
              49.398462568451485
            ],
            [
              -1.07666015625,
              49.39309989350566
            ],
            [
              -1.1370849609375,
              49.37164333826142
            ],
            [
              -1.16455078125,
              49.40561189091876
            ],
            [
              -1.30462646484375,
              49.54303352434694
            ],
            [
              -1.2908935546875,
              49.58756804188783
            ],
            [
              -1.2634277343749998,
              49.58400677536721
            ],
            [
              -1.25244140625,
              49.60715036117516
            ],
            [
              -1.23046875,
              49.61070993807422
            ]
          ]
        ]
      }
    }
  }
}];

let outputLayer = L.layerGroup();
let outputLayer2 = L.layerGroup();

let f = new Faction({name: 'The Allies', color: '#5494f9', id: 'allies', renderLayer: outputLayer});
let f2 = new Faction({name: 'Nazi Germany', color: '#ff000c', id: 'axis', renderLayer: outputLayer2});

f.consumeEvent(eventStream[0]);
f.render({'allies': f, 'axis': f2}, moment());

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoibm9haHBhcmtlciIsImEiOiJjajlnNWVjY2Yyb2R2MndwYTBuMGp6dnRvIn0.6DgQDqCg10TAdtPN5HmaOQ', {
  attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
  maxZoom: 18,
  id: 'mapbox.streets',
  accessToken: 'your.mapbox.access.token'
}).addTo(myMap);

var circle = L.circle([51.508, -0.11], {
  color: 'red',
  fillColor: '#f03',
  fillOpacity: 0.5,
  radius: 500
}).addTo(myMap);

outputLayer.addTo(myMap);
