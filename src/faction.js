const L = require('leaflet');
const _ = require('lodash');
const moment = require('moment');
const flubber = require('flubber');

class Faction {
  constructor({name, id, color, renderLayer, popUpFunction}) {
    this.id = id;
    this.name = name;
    this.color = color;

    this.popUpFunction = popUpFunction;
    this.renderLayer = renderLayer;

    this.data = {force: {}, event: {}, animation: {}};
    this.animation = {force: {}, event: {}};
    this.deleteQueue = {force: [], event: [], animation: []};
    this.renderCache = {force: {}, event: {}, animation: {force: {}, event: {}}};
    this.dirtyCache = {force: {}, event: {}, animation: []};

    this.renderLayers = {
      force: L.layerGroup(),
      event: L.layerGroup(),
      animation: L.layerGroup()
    };

    _.forOwn(this.renderLayers, (value) => {
      renderLayer.addLayer(value);
    });
  }

  static createFromState(state) {

  }

  consumeEvent(event) {
    // console.log(`Event logged on ${this.id}`);
    switch(event['type']) {
      case 'create-force':
      this.createItem('force', event['data']);
      break;
      case 'move-force':
      //debugger;
      this.createInterpolator('force', event.data['target'], this.data.force[event.data['target']], event['data'], event.data.time, event.data.end);
      this.updateItem('force', event.data['target'], event['data'], {animated: true});
      break;
      case 'update-force':
      this.updateItem('force', event['target'], event['data']);
      break;
      case 'delete-force':
      this.deleteItem('force', event.data['target']);
      break;
      case 'create-event':
      this.createItem('event', event['data']);
      break;
      case 'delete-event':
      this.deleteItem('event', event.data['target']);
      break;
    }
  }

  updateItem(type, id, ...updateDeltas) {
    if (!this.isApplicable(type, id)) {
      return;
    }

    Object.assign(this.data[type][id], ...updateDeltas, this.data[type][id], {viewer: this.id});
    this.dirtyCache[type][id] = this.data[type][id];
  }

  createInterpolator(type, id, start, end, startTime, endTime) {
    if (!this.isApplicable(type, id)) {
      return;
    }

    let polator = flubber.interpolate(start.shape.geometry.coordinates[0], end.shape.geometry.coordinates[0], {string: false});
    this.animation[type][id] = {interp: polator, targetType: type, targetId: id, start: moment(startTime).unix(), end: moment(endTime).unix()};
  }

  createItem(type, data) {
    let newItem = Object.assign({}, data, {viewer: this.id});
    let id = data.id;
    this.data[type][id] = newItem;
    this.dirtyCache[type][id] = newItem;
  }

  deleteItem(type, id) {
    if (!this.isApplicable(type, id)) {
      return;
    }

    this.deleteQueue[type].push(id);
    this.deleteQueue.animation.push({
      id: id,
      type: type
    });
  }

  isApplicable(type, id) {
    if (this.id == 'actual') {
      if (this.data[type][id] != null) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  cleanEvents(time) {
    _.forOwn(this.data.event, (event, key) => {
      if (moment(event['end-time']).isSameOrBefore(time)) {
        this.deleteItem('event', key);
      }
    });
  }

  setState(state) {

  }

  setShape(type, id, shape) {
    this.renderLayers[type].addLayer(shape);
    //Null Guard: Run remove only if their is a layer remove
    this.renderCache[type][id] && this.renderCache[type][id].remove();

    this.renderCache[type][id] = shape;
  }

  createShape(type, data, factions) {
    let layerShape = L.geoJSON(data['shape'], {
      style: {
        color: factions[data['viewer']].color,
        fillColor: (type == 'event') ? '#50' : factions[data['owner']].color
      }
    });

    if (this.popUpFunction != null && data['description'] != null) {
      layerShape.on('click', () => {
        this.popUpFunction(data['description'], data['time']);
      });
    }

    return layerShape;
  }

  resetRenderer(newRenderLayer) {
    _.forOwn(this.renderLayers, (layer) => {
      layer.remove();
    });

    this.renderLayer = newRenderLayer;

    this.renderLayers = {
      force: L.layerGroup(),
      event: L.layerGroup(),
      animation: L.layerGroup()
    };

    _.forOwn(this.renderLayers, (value) => {
      this.renderLayer.addLayer(value);
    });

    this.dirtyCache = this.data;
  }

  render(factions, time) {
    this.cleanEvents(time);
    let allShapes = [];

    for (let type of ['force', 'event']) {
      for (let id of this.deleteQueue[type]) {
        this.renderCache[type][id] && this.renderCache[type][id].remove();

        delete this.renderCache[type][id]; //Remove the force's cache entry
        delete this.dirtyCache[type][id];
        delete this.data[type][id];
      }
    }

    for (let removeData of this.deleteQueue.animation) {
      let id = removeData.id;
      let type = removeData.type;

      this.renderCache.animation[type] && this.renderCache.animation[type][id] && this.renderCache.animation[type][id].remove();
      delete this.animation[type][id];
    }

    _.forOwn(this.dirtyCache.force, (dirtyForce) => {
      if (!dirtyForce.animated) {
        let layerShape = this.createShape('force', dirtyForce, factions);
        this.setShape('force', dirtyForce.id, layerShape);
      } else {
        // console.log(`Force ${dirtyForce.id} removed.`);
        this.renderCache['force'][dirtyForce.id] && this.renderCache['force'][dirtyForce.id].remove();
      }
    });

    _.forOwn(this.dirtyCache.event, (dirtyEvent) => {
      let layerShape = this.createShape('event', dirtyEvent, factions);
      this.setShape('event', dirtyEvent.id, layerShape);
    });

    let timeInSec = time.unix();
    for (let type of ['event', 'force']) {
      _.forOwn(this.animation[type], (animation) => {
        // console.log("Animation rendered");

        let interpValue = Math.min(1, (timeInSec - animation.start) / (animation.end - animation.start));
        let shapePoints = animation.interp(interpValue);

        let geoJsonVal = {
          "type": "Feature",
          "properties": {},
          "geometry": {
            "type": "Polygon",
            "coordinates": [shapePoints]
          }
        }


        let targetData = Object.assign({}, this.data[animation.targetType][animation.targetId]);

        targetData.shape = geoJsonVal;

        let layerShape = this.createShape(animation.targetType, targetData, factions);
        this.renderCache.animation[animation.targetType] && this.renderCache.animation[animation.targetType][animation.targetId] && this.renderCache.animation[animation.targetType][animation.targetId].remove();
        this.renderCache.animation[animation.targetType][animation.targetId] = layerShape;
        this.renderLayers.event.addLayer(layerShape);

      });
    }

    //TODO Animation Rendering Code

    this.dirtyCache = {force: {}, event: {}, animation: []}; //Clear the dirty cache
    this.deleteQueue = {force: [], event: [], animation: []}; //Clear the delete queue
  }
}

module.exports = Faction;
