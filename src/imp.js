const L = require('leaflet');
const _ = require('lodash');
const moment = require('moment');
const Faction = require('./faction');

class Imp {
  constructor({eventStream, map, viewPoint, popUpFunction}, {startTime, endTime}, headless = false) {
    this.index = 0;
    this.eventStream = eventStream;
    this.factions = {};
    this.deleteQueue = [];
    this.map = map;

    this.popUpFunction = popUpFunction;

    this.startTime = startTime;
    this.endTime = endTime;

    this.headless = headless;

    if (!this.headless) {
      this.renderLayers = {};
      this.enabledLayers = {};
      this.dirtyLayers = [];
      this.outputLayer = L.layerGroup();
      this.outputLayer.addTo(map);
    }

    this.warpTime({targetTime: startTime, transitionTime: 0});
  }

  consumeEvent(event) {
    switch (event['type']) {
      case 'create-faction':
        this.createFaction(event.data);
      break;
      case 'delete-faction':
        console.error("Faction delete not yet implemented");
        console.error(event);
      break;
      default:
        if (Array.isArray(event.data.viewer)) {
          for(let targetFaction of event.data.viewer) {
            this.factions[targetFaction].consumeEvent(event);
          }
        } else if (event.data.viewer == 'all') {
          _.forOwn(this.factions, (faction) => {
            faction.consumeEvent(event);
          });
        } else {
          if (event.data.owner == null || event.data.owner == event.data.viewer) {
            this.factions['actual'].consumeEvent(event);
          }
          this.factions[event.data['viewer']].consumeEvent(event);
        }
      break;
    }
  }

  createFaction(data) {
    let id = data.id;
    if (!this.headless) {
      this.renderLayers[id] = L.layerGroup();
      data.renderLayer = this.renderLayers[id];
      data.popUpFunction = this.popUpFunction;
    }
    this.factions[id] = new Faction(data);
  }

  deleteFaction(id) {
    this.deleteQueue.push(id);
  }

  resetState() {

    this.index = 0;
    this.factions = {};
    this.deleteQueue = [];

    this.currentTime = null;

    if (!this.headless) {
      this.outputLayer && this.outputLayer.remove();
      this.renderLayers = {};
      // this.enabledLayers = {};
      this.dirtyLayers = [];
      this.outputLayer = L.layerGroup();
      this.outputLayer.addTo(this.map);
    }

    this.warpTime({targetTime: this.startTime});
    this.setFactionVisibility(this.vis);
    this.render(this.currentTime);
  }

  warpTime({targetTime, transitionTime = 0}) {
    let target = moment(targetTime);
    // console.log(`Warping to ${target.format()}`);

    if (this.currentTime != null && target.isBefore(this.currentTime)) {
      // console.log("State reseting for backwards warp");
      this.resetState();
    }

    if (transitionTime == 0) {
      while (this.index < this.eventStream.length && moment(this.eventStream[this.index]['data']['time']).isSameOrBefore(target)) {
        this.consumeEvent(this.eventStream[this.index]);
        this.index++;
      }
    } else {

    }

    this.currentTime = target;
    this.render(this.currentTime);
  }

  setFactionVisibility(visData) {
    this.vis = visData;
    _.forOwn(visData, (value, key) => {
      this.enabledLayers[key] = value;

      if (this.renderLayers[key] != null) {
        switch (value) {
          case true:
            this.outputLayer.addLayer(this.renderLayers[key]);
            this.factions[key].resetRenderer(this.renderLayers[key]);
            this.render(this.currentTime);
            break;
          case false:
            this.outputLayer.removeLayer(this.renderLayers[key]);
            delete this.renderLayers[key];
            this.renderLayers[key] = L.layerGroup();
            break;
          default:
            console.error("Invalid visibility setting");
            console.error(visData);
        }
      }

      this.dirtyLayers.push(key);
    });
  }

  render(time) {
    if (this.headless) {
      return;
    }

    _.forOwn(this.factions, (faction) => {
      faction.render(this.factions, time);
    });

    this.dirtyLayers = [];
  }

  get time() {
    return moment(this.currentTime);
  }
}

module.exports = Imp;
