let L = require('leaflet');
let Faction = require('./faction');
let Imp = require('./imp.js');
let moment = require('moment');

/*##### INITIALIZE THE MAP #####*/

let myMap = L.map('mapid', {zoomControl: false, minZoom: 3, maxZoom: 16}).setView([50.197371, -0.475703], 7);

L.tileLayer('https://api.mapbox.com/styles/v1/noahparker/cj9p4fzdt4zrh2rn2s7j84cwr/tiles/256/{z}/{x}/{y}?access_token={accessToken}',
	{
		attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
		maxZoom: 18,
		accessToken: 'pk.eyJ1Ijoibm9haHBhcmtlciIsImEiOiJjajlnNWVjY2Yyb2R2MndwYTBuMGp6dnRvIn0.6DgQDqCg10TAdtPN5HmaOQ'
	}
).addTo(myMap);

/*##### TIMELINE BOUNDS #####*/

var minDate = new moment("1944-05-01");
var maxDate = new moment("1944-07-15");
var daySpan = maxDate.diff(minDate, 'days');

/*##### INFO PANE INTERACTION #####*/

var infoPane = document.getElementById("info_pane");
var infoPaneBody = document.getElementById("info_pane_body");
var infoPaneDate = document.getElementById("info_pane_date");

var hideInfoPane = function() {
	infoPane.style.opacity = 0;
	infoPane.style.width = 0;
}

var infoPaneExit = document.getElementById("info_pane_exit");
infoPaneExit.addEventListener('click', hideInfoPane);

let displayInfoPane = function(text, date) {
	infoPane.style.opacity = 1;
	infoPane.style.width = "20%";
	infoPaneBody.innerHTML = text;
	infoPaneDate.innerHTML = moment(date).format("MMMM D, YYYY");
};

/*##### IMP INSTANCE #####*/

let eStream = require('./outputevents.json');
let imp = new Imp({eventStream: eStream, map: myMap, popUpFunction: displayInfoPane}, {startTime: minDate});
imp.setFactionVisibility({allies: true, axis: false, actual: false});

/*##### FACTION LEGENDS #####*/

var showAllies = true;
var showAxis = false;
var showActual = false;

let updateVisibility = function() {
	imp.setFactionVisibility({allies: showAllies, axis: showAxis, actual: showActual});
	imp.render(imp.time);
}

var alliesLegend = document.getElementById("legend_allies");
var alliesActive = function() {
	if (showAllies === true) {
		alliesLegend.style.opacity = 0.3;
		showAllies = false;
		alliesLegend.innerHTML = "location_off";
	}
	else {
		alliesLegend.style.opacity = 1;
		showAllies = true;
		alliesLegend.innerHTML = "location_on";
	}
	updateVisibility();
}
alliesLegend.addEventListener('click', alliesActive);

var axisLegend = document.getElementById("legend_axis");
var axisActive = function() {
	if (showAxis === true) {
		axisLegend.style.opacity = 0.3;
		showAxis = false;
		axisLegend.innerHTML = "location_off";
	}
	else {
		axisLegend.style.opacity = 1;
		showAxis = true;
		axisLegend.innerHTML = "location_on";
	}
	updateVisibility();
}
axisLegend.addEventListener('click', axisActive);

var actualLegend = document.getElementById("legend_actual");
var actualActive = function() {
	if (showActual === true) {
		actualLegend.style.opacity = 0.3;
		showActual = false;
		actualLegend.innerHTML = "location_off";
	}
	else {
		actualLegend.style.opacity = 1;
		showActual = true;
		actualLegend.innerHTML = "location_on";
	}
	updateVisibility();
}
actualLegend.addEventListener('click', actualActive);

/*### SETTINGS PANEL ###*/
var showHelp = false;
var helpIcon = document.getElementById("settings_info");
var helpContent = document.getElementById("help_content");
var toggleHelp = function() {
	if (showHelp === true) {
		helpContent.style.display = "none";
		showHelp = false;
		helpIcon.innerHTML = "info_outline";
	}
	else {
		helpContent.style.display = "flex";
		showHelp = true;
		helpIcon.innerHTML = "info";
	}
}
helpIcon.addEventListener('click', toggleHelp);

var settingsPane = document.getElementById("settings_pane");
var settingsIcon = document.getElementById("settings_options");
var settingsPaneExit = document.getElementById("settings_pane_exit");
var showSettingsPane = function() {
	settingsPane.style.opacity = 1;
	settingsPane.style.width = "20%";
}
var hideSettingsPane = function() {
	settingsPane.style.opacity = 0;
	settingsPane.style.width = 0;
}
settingsIcon.addEventListener('click', showSettingsPane);
settingsPaneExit.addEventListener('click', hideSettingsPane);

/*#################################################*/
/*################### TIMELINE ####################*/    //TODO: make separate file
/*#################################################*/

/*##### VARIABLES #####*/                  //TODO: update on browser resize

//Get timeline and container
var timeline = document.getElementById("timeline");
var timelineCont = document.getElementById("timeline_container");
var cursorFix = document.getElementById("timeline_container_cursor_drag_fix");

//Scaling  (time to px) - UPDATED ON RESIZE
var dayLen = 20; //old: 50
var minLen = dayLen/(24 * 60);

//Bounds of viewing area - UPDATED ON RESIZE
var viewWidth = timelineCont.offsetWidth;
var viewNumDays = Math.ceil(viewWidth / dayLen / 2); //Number of days visible in half of the timeline
var maxOffset = viewWidth / 2; //AKA min date
var minOffset = (viewWidth / 2) - (daySpan * dayLen); //AKA max date
if (maxOffset < minOffset) {
	maxOffset = minOffset;
}

//Current date
var currentDate = new moment(minDate);

//Necessary for redrawing
let viewDay = currentDate.diff(minDate, 'days'); //Days since start

//Timeline playback speed (simulation min / realtime sec)
var timeSpeed = 150;

//Display the current time
var showDate = false;
function displayDate() {
	if (showDate)
		dateDisplay.innerHTML = currentDate.format("kk:mm MMMM D, YYYY");
}
var option_showDate = document.getElementById("options_date");
var dateDisplay = document.getElementById("date_display");
var toggleShowDate = function() {
	if (showDate === true) {
		dateDisplay.style.display = "none";
		showDate = false;
		option_showDate.innerHTML = "check_box_outline_blank";
	}
	else {
		dateDisplay.style.display = "flex";
		showDate = true;
		option_showDate.innerHTML = "check_box";
		dateDisplay.innerHTML = currentDate.format("kk:mm MMMM D, YYYY");
	}
}
option_showDate.addEventListener('click', toggleShowDate);

/*##### DATE LINE CREATION #####*/

//Date to use when adding date lines
let tempDate = new moment(minDate);

//Initial date lines
tempDate.subtract(viewNumDays, 'days');
for (var i= -viewNumDays; i<viewNumDays; i++) {
	//Date line
		//General properties
		var timeBar = document.createElement("DIV");
		timeBar.style.position = "absolute";
		timeBar.style.top = "0px";
		timeBar.style.height = "100%";
		timeBar.style.borderLeftStyle = "solid";
		//Specific properties
		timeBar.style.left = (dayLen*i) + "px";
		timeBar.style.width = dayLen + "px";
	//Label
		var label = document.createElement("DIV");
		label.style.position = "absolute";
		label.style.bottom = "0px";
		label.style.height = "20px";
		label.style.left = (dayLen*i) + "px";
		label.style.backgroundColor = "rgb(255,255,255)";
		label.style.fontSize = "12px";
		label.style.marginLeft = "1px";
		label.style.paddingLeft = "5px";
		label.style.paddingRight = "5px";
	//Major / minor formatting
	if (tempDate.date() !== 1) {
		timeBar.style.borderLeftWidth = "1px";
		timeBar.style.borderLeftColor = "rgb(200,200,200)";
		label.innerHTML = tempDate.format('D');
	}
	else {
		timeBar.style.borderLeftWidth = "2px";
		timeBar.style.borderLeftColor = "rgb(100,100,100)";
		//Create label for first of month
		label.innerHTML = tempDate.format("MMMM");
	}
	//Add to timeline
	timeline.appendChild(label);
	timeline.appendChild(timeBar);
	tempDate.add(1, 'days');
}

//Update date lines on scaling
function updateTimelineScale() {
	//Update variables with new scale
	minLen = dayLen/(24 * 60);
	viewWidth = timelineCont.offsetWidth;
	viewNumDays = Math.ceil(viewWidth / dayLen / 2);
	maxOffset = viewWidth / 2;
	minOffset = (viewWidth / 2) - (daySpan * dayLen);
	if (maxOffset < minOffset) {
		maxOffset = minOffset;
	}
	viewDay = currentDate.diff(minDate, 'days');

	//Reset timeline content
	timeline.innerHTML = "";

	//Redraw the date lines
	tempDate = new moment(currentDate); //current date is center
	tempDate.subtract(viewNumDays, 'days'); //Start at the left of the viewing area
	for (var i= viewDay-viewNumDays; i<viewDay+viewNumDays+1; i++) {
		//Date line
			//General properties
			var timeBar = document.createElement("DIV");
			timeBar.style.position = "absolute";
			timeBar.style.top = "0px";
			timeBar.style.height = "100%";
			timeBar.style.borderLeftStyle = "solid";
			//Specific properties
			timeBar.style.left = (dayLen*i) + "px";
			timeBar.style.width = dayLen + "px";
		//Label
			var label = document.createElement("DIV");
			label.style.position = "absolute";
			label.style.bottom = "0px";
			label.style.height = "20px";
			label.style.left = (dayLen*i + 1) + "px";
			label.style.backgroundColor = "rgb(255,255,255)";
			label.style.fontSize = "12px";
			label.style.marginLeft = "1px";
			label.style.paddingLeft = "5px";
			label.style.paddingRight = "5px";
		//Major / minor formatting
		if (tempDate.date() !== 1) {
			timeBar.style.borderLeftWidth = "1px";
			timeBar.style.borderLeftColor = "rgb(200,200,200)";
			label.innerHTML = tempDate.format('D');
		}
		else {
			timeBar.style.borderLeftWidth = "2px";
			timeBar.style.borderLeftColor = "rgb(100,100,100)";
			//Create label for first of month
			label.innerHTML = tempDate.format("MMMM");
		}
		//Add to timeline
		timeline.appendChild(timeBar);
		timeline.appendChild(label);
		tempDate.add(1, 'days');
	}
	/*
	if (dayLen >= 20) //Draw DAYS LINES
	if (dayLen >= 25) //Draw DAYS NUMBERS
	if (dayLen >= 250) //Draw HOURS LINES */        // TODO: show more / less detail

	//Update cursor position according to scale
	timeline.style.left = maxOffset - currentDate.diff(minDate, 'minutes') * minLen;
}


/*##### DRAG TIMELINE #####*/

dragElement();

function dragElement() {
	//Initial x and new x
	var x1 = 0, x2 = 0;
	//Begin drag on mouse down
	timelineCont.onmousedown = dragMouseDown;

	function dragMouseDown(e) {
		e = e || window.event;
		//Get initial mouse pos
		x1 = e.clientX;
		//End drag on mouse up
		document.onmouseup = dragMouseUp;
		//Call elementDrag whenever the cursor moves:
		document.onmousemove = elementDrag;
		//Keep cursor as ew-resize
		cursorFix.style.display = "block";
	}

	function elementDrag(e) {
		e = e || window.event;
		//Calculate cursor delta
		x2 = e.clientX - x1;
		x1 = e.clientX;
		//Temp variable so orig. minDate isn't modified
		tempDate = moment(minDate);
		//Set new position WITHIN BOUDNS
		if (timeline.offsetLeft + x2 >= minOffset) {
			if (timeline.offsetLeft + x2 <= maxOffset) {
				timeline.style.left = (timeline.offsetLeft + x2) + "px";
				tempDate.add((maxOffset-timeline.offsetLeft)/minLen, 'minutes');
			}
			else {
				//Leave tempDate as minDate
				timeline.style.left = maxOffset + "px";
			}
		}
		else {
			tempDate = moment(maxDate);
			timeline.style.left = minOffset + "px";
		}
		//Update map and current date
		if(!currentDate.isSame(tempDate)) {
			currentDate = moment(tempDate);
			imp.warpTime({targetTime: currentDate});
			displayDate();
			//TEMPORARY - TODO: make more efficient for timeline scroll (add/remove beginning/end)
			updateTimelineScale();
		}
	}

	function dragMouseUp() {
		//Unbind events when mouse released
		document.onmouseup = null;
		document.onmousemove = null;
		//Reset cursor type
		cursorFix.style.display = "none";
	}
}

/*##### TIMELINE ZOOM #####*/

var totScrollDelta = 0;
var queueScroll = true;

function timelineZoom(scrollDelta) {
	dayLen -= scrollDelta;
	if (dayLen < 2.5)
		dayLen = 2.5;
	if (dayLen > 400)
		dayLen = 400;
	updateTimelineScale();
}

timelineCont.addEventListener('wheel', 
	function(e) {
		//Calculate net scroll delta in between frames
		totScrollDelta += e.deltaY;
		//If it is time for next frame, call next frame (limits number of calls to framerate)
		if (queueScroll) {
			window.requestAnimationFrame(
				function() {
					//When it is time for next frame, browser will call zoom function and reset variables
					timelineZoom(totScrollDelta);
					totScrollDelta = 0;
					queueScroll = true;
				}
			);
			queueScroll = false;
		}
	}
);

/*##### TIMELINE PLAYBACK #####*/

var play_button = document.getElementById("timeline_play");

var play = false;
let prevTimeStamp = null;

function playTimeline() {
	play_button.innerHTML = "pause";

	play = true;
	prevTimeStamp = null;

	let animateTime = function (timeStamp) {
		//Get dekta time
		let timeDelta = timeStamp - prevTimeStamp;
		//Step forward timeline
		if (prevTimeStamp !== null) {
			imp.warpTime({targetTime: currentDate.add(timeDelta * timeSpeed / 1000, 'minutes')});
			timeline.style.left = maxOffset - currentDate.diff(minDate, 'minutes') * minLen;
			displayDate();
		}
		//Store prev time
		prevTimeStamp = timeStamp;
		//Call next frame
		if (play) {
			//Check bounds - stop at ends of timeline
			if (timeSpeed > 0) { //(going forward)
				if (currentDate.isBefore(maxDate)) 
					window.requestAnimationFrame(animateTime);
				else {
					pauseTimeline();
					currentDate = moment(maxDate);
					imp.warpTime({targetTime: currentDate});
					timeline.style.left = minOffset;
				}
			}
			else { //(going backward)
				if (currentDate.isAfter(minDate))
					window.requestAnimationFrame(animateTime);
				else {
					pauseTimeline();
					currentDate = moment(minDate);
					imp.warpTime({targetTime: currentDate});
					timeline.style.left = maxOffset;
				}
			}
			//TEMPORARY - TODO: make more efficient for timeline scroll (add/remove beginning/end)
			updateTimelineScale();
		}
	}

	window.requestAnimationFrame(animateTime);
}

function pauseTimeline() {
	play_button.innerHTML = "play_arrow";

	play = false;
	prevTimeStamp = null;
}

function playPauseTime() {
	if (!play) {
		playTimeline();
	}
	else {
		pauseTimeline();
	}
}

//Chage time speed - 150, 600, 1500, 300, 7500
var forward_text = document.getElementById("forward_text");
function forwardTime() {
	if (timeSpeed < 150 || timeSpeed === 7500) {
		timeSpeed = 150;
		forward_text.innerHTML = "1x";
		backward_text.innerHTML = "";
	}
	else if (timeSpeed < 600) {
		timeSpeed = 600;
		forward_text.innerHTML = "2x";
	}
	else if (timeSpeed < 1500) {
		timeSpeed = 1500;
		forward_text.innerHTML = "3x";
	}
	else if (timeSpeed < 3000) {
		timeSpeed = 3000;
		forward_text.innerHTML = "4x";
	}
	else if (timeSpeed < 7500) {
		timeSpeed = 7500;
		forward_text.innerHTML = "5x";
	}
}

var backward_text = document.getElementById("backward_text");
function backwardTime() {
	if (timeSpeed > -150 || timeSpeed === -7500) {
		timeSpeed = -150;
		backward_text.innerHTML = "1x";
		forward_text.innerHTML = "";
	}
	else if (timeSpeed > -600) {
		timeSpeed = -600;
		backward_text.innerHTML = "2x";
	}
	else if (timeSpeed > -1500) {
		timeSpeed = -1500;
		backward_text.innerHTML = "3x";
	}
	else if (timeSpeed > -3000) {
		timeSpeed = -3000;
		backward_text.innerHTML = "4x";
	}
	else if (timeSpeed > -7500) {
		timeSpeed = -7500;
		backward_text.innerHTML = "5x";
	}
}

//Bind controls
play_button.addEventListener('click', playPauseTime);
let fast_forward = document.getElementById("timeline_forward");
fast_forward.addEventListener('click', forwardTime);
let fast_rewind = document.getElementById("timeline_backward");
fast_rewind.addEventListener('click', backwardTime);

/*##### KEYBOARD INPUT #####*/

document.body.addEventListener("keypress", function (event) {
	const keyName = event.key;
	//console.log(`Key: ${keyName} pressed.`);
	//console.log(event);
	switch (keyName) {
		case ' ':
			//Spacebar = play/pause the timeline
			playPauseTime();
		break;
		case '.':
			//Period = fast forward
			forwardTime();
		break;
		case ',':
			//Comma = rewind
			backwardTime();
		break;
		/* case ' ':
			if (!play) {
				let stepAnimation = function (timeStamp) {
					let timeDelta = timeStamp - lastRenderTime;
					lastRenderTime = timeStamp;
					imp.warpTime({targetTime: imp.time.add(timeDelta/1e3, 'hours')});
					window.requestAnimationFrame(stepAnimation);
				}

				window.requestAnimationFrame(stepAnimation);
			} else {

			}
		break; */
	}
});

/*##### NOTES #####*/
//Width of container -- offsetWidth;
